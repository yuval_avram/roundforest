import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import au.com.bytecode.opencsv.CSVReader;


public class CSVLoader {

	private Connection connection;
	//Set default separator
	private char seprator = ',';

	public CSVLoader(Connection connection) {
		this.connection = connection;
	}
	
	public int loadCSV(String csvFile) throws Exception {
		CSVReader csvReader = null;
		if(null == this.connection) {
			throw new Exception("Not a valid connection.");
		}
		try {
			
			csvReader = new CSVReader(new FileReader(csvFile), this.seprator);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error occured while executing file. "
					+ e.getMessage());
		}
		
		int count = 0;

		String[] nextLine;
		Connection con = this.connection;;
		PreparedStatement ps = null;
		try {
			con.setAutoCommit(false);
			ps = con.prepareStatement(MySqlQueries.INSERT_REVIEWS_QUERY);
			
			//delete data from table before loading csv
			System.out.println("clear data");
			con.createStatement().execute(MySqlQueries.DELETE_REVIEWS_QUERY);

			final int batchSize = 1000;
			csvReader.readNext(); //ignore headers
			while ((nextLine = csvReader.readNext()) != null) {
				if (null != nextLine) {
//					String id = nextLine[0];
					String ProductId = nextLine[1];
					String UserId = nextLine[2];
					String ProfileName = nextLine[3];
					if(UserId.length() < 45 && ProfileName.length() < 45){
//						ps.setString(1, id);
						ps.setString(1, ProductId);
						ps.setString(2, UserId);
						ps.setString(3, ProfileName);
	
						ps.addBatch();
					}
				}
				if (++count % batchSize == 0) {
					System.out.println(count);
					ps.executeBatch();
				}
			}
			ps.executeBatch(); // insert remaining records
			con.commit();
		} catch (Exception e) {
			con.rollback();
			e.printStackTrace();
			throw new Exception(
					"Error occured while loading data from file to database."
							+ e.getMessage());
		} finally {
			System.out.println("finish");
			if (null != ps)
				ps.close();
			if (null != con)
				con.close();

			csvReader.close();
		}
		return count;
	}

}