import java.io.FileReader;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;

import au.com.bytecode.opencsv.CSVReader;


public class Main {
	
	private static HashMap<String, Integer> products_map = new HashMap<String, Integer>();
	private static HashMap<String, Integer> profiles_map = new HashMap<String, Integer>();
	private static HashMap<String, Integer> words_map = new HashMap<String, Integer>();
	private static ArrayList<String> reviewTextArr = new ArrayList<String>();

	
	public static void main(String[] args) throws Exception {

        int num_of_rows = 0;
		try {
			num_of_rows = loadCSV(Config.FILE_PATH);
			
			System.out.println("Finish uploading, number of rows: "+ num_of_rows);

			System.out.println("\nPupular " + Config.NUM_OF_RESULTS +" profiles:");
		    Utils.print_map(Utils.sort_map(profiles_map));
		    
			System.out.println("\nPupular " + Config.NUM_OF_RESULTS +" products:");
		    Utils.print_map(Utils.sort_map(products_map));
			
			System.out.println("\nPupular " + Config.NUM_OF_RESULTS +" words:");
		    Utils.print_map(Utils.sort_map(words_map));
			
	    	if(args.length > 0 && "translate=true".equalsIgnoreCase(args[0])){
	    		createGoogleTranslaeThreads();
	    	}	
		    
//			int first_row_id = MySqlQueries.get_first_row_id();
//			
//			System.out.println("\nPupular " + Config.NUM_OF_RESULTS +" profiles:");
//			MySqlQueries.find_popular_users(first_row_id, num_of_rows);
//		    
//			System.out.println("\nPupular " + Config.NUM_OF_RESULTS +" products:");
//			MySqlQueries.find_popular_products(first_row_id, num_of_rows);
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
  
	}


	/**
	 * create 100 threads for sync http post request (maximum 100 calls parallel)
	 * json params are concat by '\n**\n' in order to call some review (less than 1000 characters) together
	 */
	private static void createGoogleTranslaeThreads() {
		
		System.out.println("\nStart sending requests for google translate, 100 parralel");

		// calc number of calls per thread
		int num_of_requests_per_thread=(int) Math.ceil(reviewTextArr.size()/100.0);
		
		int last_index = 0;
		for(int i=0;i<100;i++){
			
			ArrayList<String> threadReviews = new ArrayList<String>();
			
			// add reviews for thread
			for(int threadReviewCounter=0; threadReviewCounter < num_of_requests_per_thread && last_index < reviewTextArr.size(); last_index++, threadReviewCounter++){
				threadReviews.add(reviewTextArr.get(last_index));
			}
			
			// run
			Runnable googleTranslateThread = new GoogleTranslateThread(threadReviews);
			new Thread(googleTranslateThread).start();
		}
	}

	/**
	 * 
	 * @param csvFile
	 * @return rows_count
	 * @throws Exception
	 * 
	 * load csv file and calculate data
	 * 
	 * algorithm - 
	 * 		for each 50000 results:
	 * 			save it for 'temp_map'
	 * 			sort map
	 * 			merge first 2000 results into 'final map'
	 * 		finally print the first 1000 results of the 'final map'
	 * 
	 */
	private static int loadCSV(String csvFile) throws Exception {
		
		CSVReader csvReader = null;
		csvReader = new CSVReader(new FileReader(csvFile), ',');

		int rows_count = 1;
		int words_count = 0;

		HashMap<String, Integer> temp_products_map = new HashMap<String, Integer>();
		HashMap<String, Integer> temp_profiles_map = new HashMap<String, Integer>();
		HashMap<String, Integer> temp_words_map = new HashMap<String, Integer>();
		
		String[] nextLine;
//		Connection con = MySqlQueries.getCon();
		PreparedStatement ps = null;
		String concatText = "";

		try {
//			con.setAutoCommit(false);
//			ps = con.prepareStatement(MySqlQueries.INSERT_REVIEWS_QUERY);
			
			//delete data from table before loading csv
		//	System.out.println("clear data");
		//	con.createStatement().execute(MySqlQueries.DELETE_REVIEWS_QUERY);

			int batchSize = 1000;
			//ignore headers row
			csvReader.readNext(); 
			while ((nextLine = csvReader.readNext()) != null) {
				if (null != nextLine) {
//					String id = nextLine[0];
					String productId = nextLine[1].toLowerCase();
//					String userId = nextLine[2];
					String profileName = nextLine[3].toLowerCase();
					String reviewText = nextLine[9].toLowerCase();
					
					//ignore reviews > 1000 characters for translate
					if(reviewText.length() < 1000){
						if(concatText.length() + reviewText.length() + 3 < 1000){
							concatText=concatText.concat("\n**\n"+reviewText);
						} else{
							reviewTextArr.add(concatText);
							concatText = reviewText;
						}
						
					}
//					if(productId.length() < 45 && userId.length() < 45 && profileName.length() < 45){
//						ps.setString(1, id);
//						ps.setString(1, productId);
//						ps.setString(2, userId);
//						ps.setString(3, profileName);
//						ps.addBatch();
//					}
					
					//add value for temp_profiles_map
					Utils.add_value_to_map(temp_profiles_map, profileName);
					
					//add value for temp_products_map
					Utils.add_value_to_map(temp_products_map, productId);						
					
					String[] review_words = reviewText.trim().split(" ");
					for (String review_word : review_words) {
						if(!review_word.isEmpty()){
							
							//add value for temp_words_map
							Utils.add_value_to_map(temp_words_map, review_word);
							
							words_count++;	
							
							//if pass 50000 words, sort 'temp_map' and merge to 'final'
							if (++words_count % Config.MAX_MAP_SIZE == 0) {
								words_map = Utils.merge_and_sort_maps(words_map, temp_words_map);
								temp_words_map = new HashMap<String, Integer>();
	
							}
						}
					}

				}
				
				//if pass 50000 words, sort 'temp_map' and merge to 'final'
				if (rows_count % Config.MAX_MAP_SIZE == 0) {
					products_map = Utils.merge_and_sort_maps(products_map, temp_products_map);
					profiles_map = Utils.merge_and_sort_maps(profiles_map, temp_profiles_map);

					temp_profiles_map = new HashMap<String, Integer>();
					temp_products_map = new HashMap<String, Integer>();
				}

				//print every 1000 rows
				if (rows_count % batchSize == 0) {
					System.out.println(rows_count);
			//		ps.executeBatch();
				}
				rows_count++;
			}
			//ps.executeBatch(); // insert remaining records
			//con.commit();
		} catch (Exception e) {
//			con.rollback();
			e.printStackTrace();
			throw new Exception(
					"Error occured while loading data from file to database."
							+ e.getMessage());
		} finally {
			if (null != ps)
				ps.close();
//			if (null != con)
//				con.close();

			csvReader.close();
		}
		
		// merge final bulk of temp_maps
		products_map = Utils.merge_and_sort_maps(products_map, temp_products_map);
		profiles_map = Utils.merge_and_sort_maps(profiles_map, temp_profiles_map);
		words_map = Utils.merge_and_sort_maps(words_map, temp_words_map);
	    
	    return rows_count;
	}


}