import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class GoogleTranslateThread implements Runnable {

	private ArrayList<String> reviewTextArr; // name of thread
	private static String url = "https://api.google.com/translate";

	
	public GoogleTranslateThread(ArrayList<String> reviewTextArr){
	    this.reviewTextArr = reviewTextArr;
	}
	

	public void run() {
		for (String string : this.reviewTextArr) {
			try {
				String sendingPostResponse = sendingPostRequest(string);
				
				// TODO - handle respone, split by '/n**/n'
				System.out.println("Request:\n"+string+"\n"+sendingPostResponse+"\n"+"--------------------------------");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * 
	 * @param text
	 * @return respone string from google translate
	 * @throws Exception
	 */
	private String sendingPostRequest(String text) throws Exception {
		 
		String postJsonData = "{\"input_lang\":en,\"output_lang\":\"fr\",\"text\":"+text+"}";
		  
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		 
		// Setting basic post request
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type","application/json");
		 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(postJsonData);
		wr.flush();
		wr.close();
		 
		int responseCode = con.getResponseCode();

		BufferedReader in;
		if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST) {		 
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));	  
		} else {
		       /* error from server */
			in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		  
		StringBuffer response = new StringBuffer();
		String output;
	
		while ((output = in.readLine()) != null) {
			response.append(output);
		}
		in.close();
		
		return response.toString();
		  
		  
	}

}