import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


public class Utils {
	
	/**
	 * 
	 * @param list_of_maps
	 * @return HashMap<String, Integer> merged_map
	 * 
	 * merge list of <String, Integer> maps to one map, the value is the sum of all key's values
	 * 
	 */
	public static HashMap<String, Integer> merge_maps(
			ArrayList<HashMap<String, Integer>> list_of_maps) {
		HashMap<String, Integer> merged_map = new HashMap<String, Integer>();
		merged_map.putAll(list_of_maps.get(0));
	    for(int i=1; i<list_of_maps.size(); i++){
	    	list_of_maps.get(i).forEach((k, v) -> merged_map.merge(k, v, Integer::sum));
	    }
		return merged_map;
	}
	

	/**
	 * 
	 * @param map
	 * @return Object[] sorted_arr
	 * 
	 * sort map and return as sorted array
	 * 
	 */
	public static Object[] sort_map(HashMap<String, Integer> map) {
		Object[] sorted_arr = map.entrySet().toArray();
	    Arrays.sort(sorted_arr, new Comparator<Object>() {
	        public int compare(Object o1, Object o2) {
	            return ((Map.Entry<String, Integer>) o2).getValue()
	                       .compareTo(((Map.Entry<String, Integer>) o1).getValue());
	        }
	    });
		return sorted_arr;
	}
	
	/**
	 * 
	 * @param total_map
	 * @param temp_map
	 * 
	 * @return total_map
	 * 
	 * sort temp_map, get top 2000 (Config.NUM_OF_RESULTS * 2) values and merge it to total_map
	 * 
	 */
	public static HashMap<String, Integer> merge_and_sort_maps(
			HashMap<String, Integer> total_map,
			HashMap<String, Integer> temp_map) {
		HashMap<String, Integer> top_words_map = new HashMap<String, Integer>();
		Object[] sort_map = Utils.sort_map(temp_map);
		for(int i=0; i < Config.NUM_OF_RESULTS * 2; i++){
			Entry<String, Integer> entry = (Map.Entry<String, Integer>) sort_map[i];
			top_words_map.put(entry.getKey(), entry.getValue());
		}

		top_words_map.forEach((k, v) -> total_map.merge(k, v, Integer::sum));
		return total_map;
	}
  

	/**
	 * 
	 * @param map
	 * @param value
	 * 
	 * add value to hash map <String,Int>
	 */
	public static void add_value_to_map(
			HashMap<String, Integer> map, String value) {
		Integer num_of_values_in_map = map.get(value);
		if(num_of_values_in_map != null){
			map.put(value, num_of_values_in_map+1);
		}
		else{
			map.put(value, 1);
		}
	}
	
	
	/**
	 * 
	 * @param map_arr
	 * 
	 * prints first 1000 (Config.NUM_OF_RESULTS) of sorted map
	 */
	public static void print_map(Object[] map_arr) {
		int count = 0;
	    for (Object e : map_arr) {
	    	if(count < Config.NUM_OF_RESULTS) {
				Entry<String, Integer> entry = (Map.Entry<String, Integer>) e;
				System.out.println(entry.getValue() + "\t"
				        + entry.getKey());
			}
	    	count++;
	    }
	}
	
}
