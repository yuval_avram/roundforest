
public class Config {
	
	public static String FILE_PATH = ".\\Reviews.csv";
	public static int MAX_MAP_SIZE = 50000;
	public static int NUM_OF_RESULTS = 1000;

	public static String SCHEMA_NAME = "rf";
	public static String USER = "root";
	public static String PASSWORD = "1234";
	public static String DB_CONNECTION_STRING = "jdbc:mysql://localhost/" + SCHEMA_NAME + "?user=" + USER + "&password=" + PASSWORD;

}
