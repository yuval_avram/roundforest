import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;


public class MySqlQueries {
	
	public static String INSERT_REVIEWS_QUERY = "INSERT INTO amazon_food_reviews (ProductId,UserId,ProfileName) VALUES (?,?,?)";
	public static String DELETE_REVIEWS_QUERY = "DELETE FROM amazon_food_reviews";
	public static String GET_FIRST_REVIEW_ID_QUERY = "SELECT MIN(id), count(*) FROM amazon_food_reviews";

	/**
	 * 
	 * @return DB connection
	 * 
	 * create DB connection
	 */
	public static Connection getCon() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(Config.DB_CONNECTION_STRING);
	
		} catch (SQLException ex) {
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}
	
		return conn;
	}
	
	
	/**
	 * 
	 * @param first_row_id
	 * @param num_of_rows
	 * @throws SQLException
	 * 
	 * find most popular users (first 1000) amazon_food_reviews
	 * 
	 */
	public static void find_popular_users(int first_row_id, int num_of_rows) throws SQLException {
		ArrayList<HashMap<String, Integer>> list = new ArrayList<HashMap<String, Integer>>();
		for(int i=first_row_id; i< first_row_id + num_of_rows + Config.MAX_MAP_SIZE; i+=Config.MAX_MAP_SIZE){
		    list.add(MySqlQueries.get_popular_users(i, i+50000));
			
		}
	    HashMap<String, Integer> map = Utils.merge_maps(list);
	    Utils.print_map(Utils.sort_map(map));
	}


	/**
	 * 
	 * @param first_row_id
	 * @param num_of_rows
	 * @throws SQLException
	 * 
	 * find most popular products (first 1000) from amazon_food_reviews table
	 */
	public static void find_popular_products(int first_row_id, int num_of_rows) throws SQLException {
		ArrayList<HashMap<String, Integer>> list = new ArrayList<HashMap<String, Integer>>();
		for(int i=first_row_id; i< first_row_id + num_of_rows + Config.MAX_MAP_SIZE; i+=Config.MAX_MAP_SIZE){
		    list.add(MySqlQueries.get_popular_products(i, i+Config.MAX_MAP_SIZE));
			
		}
	    HashMap<String, Integer> map = Utils.merge_maps(list);
	    Utils.print_map(Utils.sort_map(map));
	}
	
	/**
	 * 
	 * @return int first_row_id
	 * @throws SQLException
	 */
	public static int get_first_row_id() throws SQLException {
		int first_row_id = 0;
		Statement stmt = null;
		try {
	        stmt = getCon().createStatement();
	        ResultSet rs;
	        rs = stmt.executeQuery(GET_FIRST_REVIEW_ID_QUERY);

	        while (rs.next()) {

	            first_row_id = rs.getInt("MIN(id)");
	        }

	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	        if (stmt != null) { 
	        	stmt.close(); 
        	}
	    }
		
		return first_row_id;
	}
	

	/**
	 * 
	 * @param start
	 * @param end
	 * @return HashMap<String, Integer> map
	 * @throws SQLException
	 * 
	 * get popular users between start id and end id from amazon_food_reviews
	 */
	public static HashMap<String, Integer> get_popular_users(int start, int end) throws SQLException {
		Statement stmt = null;
	    String query_most_active_users = 
	    		  " SELECT COUNT(Id) cnt, ProfileName "
	    		+ " FROM amazon_food_reviews where id > "+ start+" and id < "+end
	    		+ " GROUP BY UserId"
	    		+ " ORDER BY cnt DESC"
	    		+ " LIMIT 1500";
	    HashMap<String, Integer> map = new HashMap<String, Integer>();

		try {
	        stmt = getCon().createStatement();
	        ResultSet rs;
	        rs = stmt.executeQuery(query_most_active_users);

	        while (rs.next()) {

	            //String UserId = rs.getString("UserId");
	            String ProfileName = rs.getString("ProfileName");
	            int count = rs.getInt("cnt");
	            //System.out.println(count + "\t" + ProfileName);
	        	map.put(ProfileName, count);

	        }

	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	        if (stmt != null) { 
	        	stmt.close(); 
        	}
	    }
		
		return map;
	}
  
  
	/**
	 * 
	 * @param start
	 * @param end
	 * @return HashMap<String, Integer> map
	 * @throws SQLException
	 * 
	 * get popular users between start id and end id from amazon_food_reviews
	 * 
	 */
	public static HashMap<String, Integer> get_popular_products(int start, int end) throws SQLException {
		Statement stmt = null;
	    String query_most_active_users = 
	    		  " SELECT COUNT(Id) cnt, ProductId "
	    		+ " FROM amazon_food_reviews where id > "+ start+" and id < "+end
	    		+ " GROUP BY ProductId"
	    		+ " ORDER BY cnt DESC"
	    		+ " LIMIT 1500";
	    HashMap<String, Integer> map = new HashMap<String, Integer>();

		try {
	        stmt = getCon().createStatement();
	        ResultSet rs;
	        rs = stmt.executeQuery(query_most_active_users);

	        while (rs.next()) {

	            //String UserId = rs.getString("UserId");
	            String ProductId = rs.getString("ProductId");
	            int count = rs.getInt("cnt");
	            //System.out.println(count + "\t" + ProfileName);
	        	map.put(ProductId, count);

	        }

	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	        if (stmt != null) { 
	        	stmt.close(); 
        	}
	    }
		
		return map;
	}
	
  
}
